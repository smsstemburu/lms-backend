const express = require("express");
const router = express.Router();
const userSchemaControllers = require("../controllers/userSchemaControllers.js");

// Router to get all the users data from userSchema collection - http://localhost:8000/getData
router.get("/getData", userSchemaControllers.getAllUsers);

// Router to register new users in userSchema collection - http://localhost:8000/store-data
router.post("/store-data", userSchemaControllers.registerUsers);

// Router to handle login functionality of the user - http://localhost:8000/login
router.post("/login", userSchemaControllers.loginUsers);

module.exports = router;

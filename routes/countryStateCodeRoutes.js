const express = require("express");
const router = express.Router();
const countryStateCodeControllers = require("../controllers/countryStateCodeControllers.js");

router.post("/getCountryCodes", countryStateCodeControllers.countryCodesData);

router.post("/getPinCodes", countryStateCodeControllers.statePinCodes);

module.exports = router;

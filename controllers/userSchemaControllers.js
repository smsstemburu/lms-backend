const uniqid = require("uniqid");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
const userSchema = require("../models/userSchema.js");

dotenv.config();

exports.getAllUsers = async (req, res) => {
  try {
    const data = await userSchema.find();
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.registerUsers = async (req, res) => {
  const oldUser = await userSchema.findOne({ email: req.body.email });
  if (oldUser) {
    return res.status(409).send("User Already Exist. Please Login");
  }
  req.body.userId = uniqid.time();
  const user = new userSchema(req.body);
  const token = jwt.sign({ id: user._id }, process.env.TOKEN_SECRET, {
    expiresIn: "2h",
  });
  user.accessToken = token;
  console.log(req.body);
  console.log(user);
  user.save(function (err, data) {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(" saved to database");
    }
  });
};

exports.loginUsers = async (req, res) => {
  try {
    const data = await userSchema.find({
      userId: req.body.userId,
      password: req.body.password,
    });
    if (data.length != 0) {
      const token = jwt.sign({ id: data._id }, process.env.TOKEN_SECRET, {
        expiresIn: "2h",
      });
      await userSchema.findByIdAndUpdate(data._id, { token });
      res.json({
        token,
        fullName: data[0].firstName + data[0].lastName,
        email: data[0].email,
      });
    } else {
      res.status(400).json({ message: "Please check the details entered" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.updateUser = async (req, res, next) => {
  try {
    const update = req.body;
    const userId = req.params.userId;
    await userSchema.findByIdAndUpdate(userId, update);
    const user = await userSchema.findById(userId);
    res.status(200).json({
      data: user,
      message: "User has been updated",
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteUser = async (req, res, next) => {
  try {
    const userId = req.params.userId;
    await userSchema.findByIdAndDelete(userId);
    res.status(200).json({
      data: null,
      message: "User has been deleted",
    });
  } catch (error) {
    next(error);
  }
};

const countryCodes = require("country-codes-list");
var pincodeDirectory = require("india-pincode-lookup");

exports.countryCodesData = (req, res) => {
  try {
    const myCountryCodesObject = countryCodes.customList(
      "countryNameEn",
      "{countryCallingCode}"
    );
    res.json(myCountryCodesObject);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.statePinCodes = (req, res) => {
  try {
    const pCode = req.body.pinCode;
    const pinCodes = pincodeDirectory.lookup(pCode);
    res.json(pinCodes);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

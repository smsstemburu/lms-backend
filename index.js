const express = require("express");
const cors = require("cors");
const dotenv = require("dotenv");
const jwt = require("jsonwebtoken");
const cookieParser = require("cookie-parser");
const dbConnection = require("./models/dbConnection.js");
const userSchemaRoutes = require("./routes/userSchemaRoutes.js");
const leaveSchemaRoutes = require("./routes/leaveSchemaRoutes.js");
const countryStateCodeRoutes = require("./routes/countryStateCodeRoutes.js");
const userSchema = require("./models/userSchema.js");
const leavesSchema = require("./models/leavesSchema.js");

dotenv.config();

const app = express();

app.use(cors());
app.use(express.json());
app.use(cookieParser());
app.use("/", userSchemaRoutes, countryStateCodeRoutes, leaveSchemaRoutes);

app.listen(process.env.PORT, () => {
  console.log("Server is up and running at port,", process.env.PORT);
});

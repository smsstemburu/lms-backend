const mongoose = require("mongoose");

const users = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    default: "employee",
    enum: ["employee", "admin"],
  },
  password: {
    type: String,
    required: true,
  },
  accessToken: {
    type: String,
  },
});

const usersSchema = mongoose.model("users", users);

module.exports = usersSchema;

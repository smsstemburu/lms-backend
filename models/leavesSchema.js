const mongoose = require("mongoose");

const leaves = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  fullName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  leaveFrom: {
    type: Date,
    required: true,
  },
  leaveTo: {
    type: Date,
    required: true,
  },
  leaveReason: {
    type: String,
    required: true,
  },
  leaveStatus: {
    type: String,
    default: "pending",
    enum: ["approved", "pending", "rejected"],
  },
  manager: {
    type: String,
    required: true,
  },
  managerUserId: {
    type: String,
    required: true,
  },
});

const leavesSchema = mongoose.model("leaves", leaves);

module.exports = leavesSchema;

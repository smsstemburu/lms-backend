const mongoose = require("mongoose");
const url =
  "mongodb+srv://m001-student:Monica123@sandbox.8u7o5ct.mongodb.net/LMS?retryWrites=true&w=majority&ssl=true";

const mongoParams = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

mongoose.set("strictQuery", false);
mongoose
  .connect(url, mongoParams)
  .then(() => {
    console.log("Database connection successfull");
  })
  .catch((err) => {
    console.log("Error : ", err);
  });

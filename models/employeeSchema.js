const mongoose = require("mongoose");

const adminSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    default: employee,
  },
  password: {
    type: String,
    required: true,
  },
});

const employeeUser = mongoose.model("employee", employeeSchema);

module.exports = employeeUser;

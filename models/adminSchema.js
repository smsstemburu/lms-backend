const mongoose = require("mongoose");

const adminSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    default: admin,
  },
  password: {
    type: String,
    required: true,
  },
});

const adminUser = mongoose.model("admin", adminSchema);

module.exports = adminUser;

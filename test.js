const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();
const signed = jwt.sign({ foo: "bar" }, "secret");
console.log(signed);
var decoded = jwt.verify(signed, "secret");
console.log(decoded.foo); // bar
// var decoded = jwt.verify(
//   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NzY5Nzc1NjksImV4cCI6MTY3Njk4NDc2OX0.XXt55r8M2MOeDCpMx5VULpbtaUCgNyIkZButTUMYYRo",
//   process.env.TOKEN_SECRET
// );
//console.log(decoded.foo); // bar
